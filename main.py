import argparse
import concurrent
import os
from concurrent.futures.thread import ThreadPoolExecutor

import requests
from selenium import webdriver

from pages.main_page import MainPage
from pages.report_page import ReportPage

parser = argparse.ArgumentParser(description='Download catastro reports')
parser.add_argument('-p', '--ped', help='PED number', type=str, default='3402')
parser.add_argument('-d', '--download-dir', help='Full path download directory', type=str, default=os.getcwd())
parser.add_argument('-w', '--max-workers', help='Maximum number of workers', type=int, default=3)
parser.add_argument('-hl', '--headless', help='Headless mode', type=bool, default=False,
                    action=argparse.BooleanOptionalAction)
parser.add_argument('-l', '--account-limit', help='Limit of reports to download', type=int, default=None)
args = parser.parse_args()

PED = args.ped
DOWNLOAD_DIR = args.download_dir
MAX_WORKERS = args.max_workers
HEADLESS = args.headless
LIMIT = args.account_limit

# Iniciar programa

print('Running program with the following parameters\n')
print('\n'.join([f'{key} -> {value}' for key, value in vars(args).items()]))
print()

# Obtener números de cuenta de parcelas de una pedanía

parcelas_api = 'https://idecor-ws.mapascordoba.gob.ar/geoserver/idecor/ows'
parcelas_params = {
    'service': 'WFS',
    'version': '1.0.0',
    'request': 'GetFeature',
    'typeName': 'idecor:parcelas',
    'outputFormat': 'json',
    'cql_filter': f'ped_nomenclatura={PED}'
}

response = requests.get(parcelas_api, params=parcelas_params)
body = response.json()
account_numbers = [feature.get('properties').get('Nro_Cuenta') for feature in body.get('features')]
if LIMIT:
    account_numbers = account_numbers[:LIMIT]

print(f'Account numbers: {", ".join(str(v) for v in account_numbers)}')

# Acceder al navegador y descargar informes

download_dir = DOWNLOAD_DIR
options = webdriver.ChromeOptions()
profile = {
    'download.default_directory': download_dir,
    'download.prompt_for_download': False,
    'download.directory_upgrade': True,
    'plugins.plugins_disabled': ['Chrome PDF Viewer'],
    'plugins.always_open_pdf_externally': True
}
options.add_experimental_option('prefs', profile)

if HEADLESS:
    options.add_argument('--headless')


def download_report(acc_number: int):
    ok = True
    stacktrace = None
    driver = webdriver.Chrome('drivers/chromedriver', options=options)
    driver.maximize_window()
    try:
        main_page = MainPage(driver)
        main_page.go_to_target_page()
        main_page.go_to_report_page(acc_number)
        report_page = ReportPage(driver)
        report_page.download_report()
    except Exception as e:
        ok = False
        stacktrace = e
    finally:
        driver.quit()
    return 'OK' if ok else f'FAILURE: {stacktrace}'


with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
    futures = {executor.submit(download_report, account): account for account in account_numbers}
    for future in concurrent.futures.as_completed(futures):
        print(f'Account {futures[future]} {future.result()}')
