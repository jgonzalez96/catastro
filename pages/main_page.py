import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.base_page import BasePage


class MainPage(BasePage):

    MAIN_PAGE_URL = 'https://gn-idecor.mapascordoba.gob.ar/maps/15/view'

    SEARCH_INPUT_CSS_SELECTOR = '#textToSearchId'
    SEARCH_BUTTON_CSS_SELECTOR = '#buttonGeocoderComboBoxIDECOR'
    COMBO_OPTION_XPATH = '/html[1]/body[1]/div[5]/select[1]/option[1]'
    REPORT_LINK_TEXT = 'INFORME DEL INMUEBLE'

    @property
    def search_input(self):
        return self.driver.find_element_by_css_selector(self.SEARCH_INPUT_CSS_SELECTOR)

    @property
    def search_button(self):
        return self.driver.find_element_by_css_selector(self.SEARCH_BUTTON_CSS_SELECTOR)

    @property
    def combo_option(self):
        return WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, self.COMBO_OPTION_XPATH))
        )

    @property
    def link_to_report(self):
        return WebDriverWait(self.driver, 20).until(
            expected_conditions.element_to_be_clickable((By.LINK_TEXT, self.REPORT_LINK_TEXT))
        )

    def go_to_target_page(self):
        self.driver.get(self.MAIN_PAGE_URL)

    def go_to_report_page(self, account_number: int):
        self.search_input.send_keys(account_number)
        self.search_button.click()
        self.combo_option.click()
        self.combo_option.click()
        self.link_to_report.click()
        time.sleep(20)
        self.driver.switch_to.window(self.driver.window_handles[1])
