import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.base_page import BasePage


class ReportPage(BasePage):

    DOWNLOAD_REPORT_BUTTON = '#btnGenerarReporteSinCidi'
    PRINT_BUTTON = '#lblpdf'

    @property
    def download_button(self):
        return WebDriverWait(self.driver, 30).until(
            expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, self.DOWNLOAD_REPORT_BUTTON))
        )

    @property
    def print_button(self):
        return WebDriverWait(self.driver, 30).until(
            expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, self.PRINT_BUTTON))
        )

    def download_report(self):
        self.download_button.click()
        self.print_button.click()
        time.sleep(10)
